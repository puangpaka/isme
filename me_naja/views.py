from django.shortcuts import render

# Create your views here.
def index(req):
    return render(req,'index.html')

def about_us(req):
    return render(req,'about_us.html')

def portfolio(req):
    return render(req,'portfolio.html')

def portfolio_details(req):
    return render(req,'portfolio_details.html')

def elements(req):
    return render(req,'elements.html')

def blog(req):
    return render(req,'blog.html')

def blog_details(req):
    return render(req,'single_blog.html')

def contact(req):
    return render(req,'contact.html')