"""isme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from me_naja import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index),
    path('about_us/',views.about_us),
    path('portfolio/',views.portfolio),
    path('portfolio_details/',views.portfolio_details),
    path('elements/',views.elements),
    path('blog/',views.blog),
    path('blog_details/',views.blog_details),
    path('contact/',views.contact)
]
